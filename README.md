# Test data-engineer python
  * Reply to the email with your project as a ZIP archive containing only the code (no .git, no environment).
 
# Test
Some really important tips:
 * tests are mandatory (comments are appreciated)
 * Prioritize performance and scalability 
 * Use Polars or DuckDB (you may use other packages, but you must prove they are more efficient).
 * You might need pytest (and you can use any other package)
 * You can use internet
 
*tips, the polars docs is here: https://docs.pola.rs/api/python/stable/reference/index.html*

### Exercice 1 (10 min)

In the `technical_test/data` directory you'll find a compressed json file named "products.json.gz".
That file contains a list of products defined as objects, each product object contains infomation relevant to a specific product.

**An example product object is as follows:**

```json
{  
    "idappcat" : "2076,3B,19C,138D,NULL,NULL",
    "refproduitenseigne" : 364980,
    "libelle" : "Nature bio champignons émincés 230g ",
    "categorieenseigne" : ""
}
```
  1) Read the json file. Parse it and store it in a variable called df_product
  2) Write a function called `clean_cat` that take the dataframe and remove the NULL fields of each category
  (*For example when calling clean_cat on "2076,3B,19C,138D,NULL,NULL" the output should be "2076,3B,19C,138D".*)

## Exercise 2 (15 min)
+    **The files are located in technical_test/data**
+    **There is data for two shops and one file with an extract of our asset data repository**


1) Import raw data from shops

|refenseigne|refmagasin|categorieenseigne|prixproduit|prixunitaireproduit|prixproduitsansreduction|identifiantproduit|promotion|promotiontexte|ean|position|disponible|nouveau|
|---|---|---|---|---|---|---|---|---|---|---|---|---|
|8|3880|"Offres spéciales\|Promotions"|5.85|0.15|9.75|85061|1|Réduction immédiate de 40%|NaN|1|NaN|NaN|
|8|3880|"Offres spéciales\|Promotions"|2.17|9.77|4.35|51605|1|Réduction immédiate de 50%|NaN|2|NaN|NaN|
|8|3880|"Offres spéciales\|Promotions"|5.80|11.60|5.80|61289|1|Le 3ème est gratuit, profitez-en !|NaN|3|NaN|NaN|
|8|3880|"Offres spéciales\|Promotions"|5.80|11.60|5.80|78386|1|Le 3ème est gratuit, profitez-en !|NaN|4|NaN|NaN|
|8|3880|"Offres spéciales\|Promotions"|2.32|4.64|4.65|3700|1|Réduction immédiate de 50%|NaN|5|NaN|NaN|

+ **refenseigne => Retailer's id**
+ **refmagasin => Shop's id**
+ **categorieenseigne => Retailer's products' categorization - Category where the product was scrapped**
+ **prixproduit => Product price in the shop**
+ **prixunitaireproduit => Unit price of the product in the shop** 
+ **prixproduitsansreduction => Price of the product in the shop whithout discount** 
+ **identifiantproduit => Product's id in the shops**
+ **promotion => Dataimpact's promotion id**
+ **promotiontexte => Product's promotional text in the shop**
+ **ean => Product's EAN**
+ **position => Product's position on the scrapped page** 
+ **disponible => Product's availability**
+ **nouveau => Has the product been scrapped on the "New products" page of the shops** 

2) Import our asset data repository

|pe_ref_in_enseigne | pe_id | p_id_cat
|---|---|---|
|35143 | 400479 | 1481E|
|7428 | 413257 | 116D|
|19197 | 413258 |1318E|
|6420 | 413259 | 2003E|
|24668 | 413260 | 253D|

+ **pe_ref_in_enseigne => Products id in the shops**
+ **pe_id => Dataimpact's products id**
+ **p_id_cat => Dataimpact's products' categorization's id**

3) Merge the data from the shops with the data from our asset data repository. (write the `import_raw_data` and `process_data` functions)

## Exercise 3 (5 min)
Average the prices of the products from the two days of the shop you processed. (write the `average_prices` function)

    The resulting file should be a CSV with the id of the products in one column and the price average in another one


## Exercise 4 (10 min)
Count how many products there are in each Dataimpact category and each retailer's category by day. (write the `count_products_by_categories_by_day` function)
Average the numbers of products by Dataimpact category and by retailer category on both days. (write the `average_products_by_categories` function)


## Exercise 5 (10 min)
We want to compare categories over the average price of the position ten by ten. (use the file 17-10-2018)
So the output should look like:

|category                    | 1-10| 11-20| 21-30 | ... |
|---|---|---|---|---|
|Offres spéciales Promotions | 3.4 | 1.2  | 2.1 | ... |
|Bio Epicerie sucrée Bio     | 2.2 | 2.2  | 0.2 | ... |

